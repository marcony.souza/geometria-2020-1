package codigos;
	
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalcHipotenusaSteps {

	TriangWithSelenium triang;


	@Given("iniciei a atividade de calculo de triangulos")
	public void inicar() {
		triang.getIn();
	}

	@When("Escolho o calculo de Hipotenusa")
	public void escolherHipotenusa() {
		triang.selectHipotenusa();
	}

	@When("informo o $cateto para c1")
	public void informC1(String c1) {
		triang.cateto1(c1);
	}


	@When("informo o $cateto2 para c2")
	public void informC2(String c2) throws InterruptedException {
		triang.cateto2(c2);
	}

	@When("o calculo est� sendo feito")
	public void calcular() {
		triang.calc();
	}

	@When("hipotenusa calculada � $hipotenusa")
	public void verify(String h) {
		String result = triang.getHipotenusa();
		System.out.println("\n " + result );
		Assert.assertEquals( h, result);
	}

}


