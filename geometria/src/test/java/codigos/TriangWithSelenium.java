package codigos;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TriangWithSelenium {
	public static WebDriver driver;
	
	@Test
	public void getIn() {
		//TODO 
		//baixar o chromedriver e por o path do seu computador, ou usar o geckodriver e fazer o mesmo.
	
		System.setProperty("webdriver.chrome.driver", "C:/Users/kelvin/git/geometria-2020-1/geometria/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("C:/Users/kelvin/git/geometria-2020-1/geometria/src/main/webapp/triangulo.html");
		
		}
	
	@Test
	public void cateto1(String c1) {
		WebElement inputC1 = driver.findElement(By.id("cateto1"));
		inputC1.sendKeys(c1);

	}

	@Test
	public void cateto2(String c2) throws InterruptedException {
		WebElement inputC2 = driver.findElement(By.id("cateto2"));	
		inputC2.sendKeys(c2);
		Thread.sleep(10500);
	}
	
	@Test
	public void hipotenusa(String h) {
		WebElement inputH = driver.findElement(By.id("hipotenusa"));
		inputH.sendKeys(h);
	}

	@Test
	public void calc() {
		WebElement button = driver.findElement(By.id("calcularBtn"));	
		button.click();
	}
	
	@Test
	public void selectHipotenusa() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Hipotenusa");

	}
	
	@Test
	public void selectCateto() {
		Select select = new Select(driver.findElement(By.id("tipoCalculoSelect")));
		select.selectByVisibleText("Cateto");

	}

	@Test
	public String getHipotenusa() {
		String h = "";
		WebElement hValue = driver.findElement(By.id("hipotenusa"));
		h = hValue.getText();
		return h;
	}

	@Test
	public String getCateto() {
		String c = "";
		WebElement cValue = driver.findElement(By.id("cateto2"));
		c = cValue.getText();
		return c;
	}
	
	@AfterAll
	public static void close() {
		driver.quit();
	}
}
