package codigos;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalcCatetoSteps {
	
	TriangWithSelenium triang;
	
	@Given("estou na funcionalidade de c�lculo de tri�ngulos")
	public void inicar() {
        triang.getIn();
	}

	@When("Seleciono o tipo de c�lculo Cateto")
	public void chooseCalc() {
        triang.selectCateto();
	}

	@When("informo $cateto para c1")
	public void informarCateto1(String c1) {
		triang.cateto1(c1);
	}

	@When("informo $hipotenusa para hipotenusa ")
	public void informHipotenusa(String h) {
		triang.hipotenusa(h);;
	}
	
	@When("solicito que o c�lculo seja realizado")
	public void calc() {
		triang.calc();
	}
	
	@Then("cateto2 calculado, o valor � $cateto2")
	public void verificarValor(String c2) {
		Assert.assertEquals(c2, triang.getHipotenusa());
	}

}
