package codigos;

import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CalcFirefoxTest extends TriangWithSelenium {

	@BeforeAll
	public static void setup() {
		System.setProperty("webdriver.gecko.driver","./geckodriver.exe");
		driver = new FirefoxDriver();
	}

}
